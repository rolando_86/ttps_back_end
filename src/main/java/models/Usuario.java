package models;

import java.math.BigDecimal;
import java.util.List;

import jakarta.persistence.*;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "Usuario")
public class Usuario {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "IDUsuario")
    private Long idUsuario;

    @Column(name = "nombreUsuario")
    private String nombreUsuario;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "nombres")
    private String nombres;

    @Column(name = "email")
    private String email;

    @Column(name = "contrasena")
    private String contrasena;

    @Column(name = "saldoTotal")
    private BigDecimal saldoTotal;

    @OneToMany(mappedBy="usuario")
    private List<Pago> pago;

    @OneToMany(mappedBy="idUsuarioSeguidor")
    private List<Seguir> seguir;

    @ManyToMany
    private List<GrupoAmigos> grupoAmigos;



    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}