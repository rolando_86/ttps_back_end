package models;

import jakarta.persistence.*;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "CategoriaGastos")
@Getter
@Setter
@RequiredArgsConstructor
public class CategoriaGastos {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "IDCategoriaGastos")
    private Long idCategoriaGastos;

    @Column(name = "nombreTipoGasto")
    private String nombreTipoGasto;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}
