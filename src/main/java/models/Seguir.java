package models;

import jakarta.persistence.*;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Seguir")
@Getter
@Setter
@RequiredArgsConstructor
public class Seguir {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDSeguir")
    private Long idSeguir;

    @ManyToOne
    @JoinColumn(name="IDUsuario")
    private Usuario idUsuarioSeguidor;

    @Column(name = "IDUsuarioSeguido")
    private Long idUsuarioSeguido;



    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}