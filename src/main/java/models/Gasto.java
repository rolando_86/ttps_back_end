package models;

import jakarta.persistence.*;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "Gasto")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@RequiredArgsConstructor
public class Gasto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDGasto")
    private Long idGasto;

    @Column(name = "montoGasto")
    private BigDecimal montoGasto;

    @Column(name = "fechaGasto")
    @Temporal(TemporalType.DATE)
    private Date fechaGasto;

    @Column(name = "imagenAsociada")
    private String imagenAsociada;

    @Column(name = "formaDividirGasto")
    private String formaDividirGasto;
    
    @Column(name = "IdCategoriaGastos")
    private Long idCategoriaGastos;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}