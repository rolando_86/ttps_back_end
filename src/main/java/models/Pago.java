package models;

import java.math.BigDecimal;

import jakarta.persistence.*;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Pago")
@Getter
@Setter
@RequiredArgsConstructor
public class Pago {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDPago")
    private Long idPago;

    @Column(name = "montoPagado")
    private BigDecimal montoPagado;

    @Column(name = "IDGrupo")
    private Long idGrupo;

    @Column(name = "IDUsuarioEmisor")
    private Long idUsuarioEmisor;


    @Column(name = "IDUsuarioReceptor")
    private Long idUsuarioReceptor;

    @ManyToOne
    @JoinColumn(name="IDUsuario")
    private Usuario usuario;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}