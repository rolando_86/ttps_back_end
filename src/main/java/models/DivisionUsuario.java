package models;

import java.math.BigDecimal;

import jakarta.persistence.*;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "DivisionUsuario")
@Getter
@Setter
@RequiredArgsConstructor
public class DivisionUsuario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDDivisionUsuario")
    private Long idDivisionUsuario;

    @Column(name = "IDUsuario")
    private Long idUsuario;

    @Column(name = "Valor")
    private BigDecimal valor;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}