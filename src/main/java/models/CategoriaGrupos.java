package models;
import jakarta.persistence.*;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "CategoriaGrupos")
@Getter
@Setter
@RequiredArgsConstructor
public class CategoriaGrupos {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDCategoriaGrupos")
    private Long idCategoriaGrupos;

    @Column(name = "nombreTipoGrupo")
    private String nombreTipoGrupo;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}