package models;

import jakarta.persistence.*;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "GrupoAmigos")
@Getter
@Setter
@RequiredArgsConstructor
public class GrupoAmigos {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDGrupoAmigos")
    private Long idGrupoAmigos;

    @Column(name = "nombreGrupo")
    private String nombreGrupo;

    @Column(name = "IDCategoria")
    private Long idCategoria;

    @ManyToMany(mappedBy = "grupoAmigos")
    private List<Usuario> usuarios;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}