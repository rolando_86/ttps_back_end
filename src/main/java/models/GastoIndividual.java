package models;

import jakarta.persistence.*;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "GastoIndividual")
@Getter
@Setter
@RequiredArgsConstructor
public class GastoIndividual extends Gasto{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDGastoIndividual")
    private Long idGastoIndividual;

    @Column(name = "IDUsuarioEmisor")
    private Long idUsuarioEmisor;

    @Column(name = "IDUsuarioReceptor")
    private Long idUsuarioReceptor;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}
