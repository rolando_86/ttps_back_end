package com.ttps.CuentasClarastp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CuentasClarastpApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuentasClarastpApplication.class, args);
	}

}
