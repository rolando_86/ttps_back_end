package com.ttps.CuentasClarastp.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = "com.ttps.CuentasClarastp.repository")
@ComponentScan(basePackages = { "com.ttps.CuentasClarastp.*" })
@EntityScan(basePackages = "com.ttps.CuentasClarastp.modelsDB")
public class JpaConfig {
    // Configuración adicional si es necesario
}
