package com.ttps.CuentasClarastp.controller;

import com.ttps.CuentasClarastp.modelsDB.*;
import com.ttps.CuentasClarastp.modelsDB.DTO.GastoDTO;
import com.ttps.CuentasClarastp.modelsDB.DTO.GrupoDto;
import com.ttps.CuentasClarastp.modelsDB.DTO.SolicitudDTO;
import com.ttps.CuentasClarastp.service.*;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CuentasClarasController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private CategoriaGruposService categoriaGruposService;

    @Autowired
    private CategoriaGastosService categoriaGastosService;

    @Autowired
    private GrupoAmigosService grupoAmigosService;

    @Autowired
    private GastoService gastoService;

    @Autowired
    private SolicitudService solicitudService;


    @GetMapping("/allUsers")
    public ResponseEntity<List<Usuario>> getAllUser(){
        List<Usuario> list = new ArrayList<>();
        Usuario user = new Usuario();
        user.setNombres("rolando");
        list.add(user);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PostMapping("/usuario")
    public ResponseEntity<Object> postCrearUsuario(@RequestBody Usuario usuario){
        try {
            Usuario usuarioResponse = usuarioService.postCreateUsuario(usuario);
            return new ResponseEntity<>(usuarioResponse, HttpStatus.CREATED);
        }catch (DataIntegrityViolationException e) {
                // Manejar la excepción de violación de unicidad
            return new ResponseEntity<>("Error: El correo electrónico ya está registrado.", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
                // Manejar otras excepciones
            return new ResponseEntity<>("Error interno del servidor", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/usuario/login")
    public ResponseEntity<Object> getLogin(@RequestBody Login login){
        Usuario usuarioResponse = usuarioService.getLogin(login);
        if (usuarioResponse == null) {
            return new ResponseEntity<>("Error: El email y contrasena no se encuentra en el sistema", HttpStatus.FORBIDDEN);
        }
        return new ResponseEntity<>(usuarioResponse, HttpStatus.OK);
    }

    @GetMapping("/CategoriaGrupo")
    public ResponseEntity<List<CategoriaGrupos>> getCategoriaGrupos(){
        List<CategoriaGrupos> listCategoriaGrupos = categoriaGruposService.getCategoriasGrupos();
        return new ResponseEntity<>(listCategoriaGrupos, HttpStatus.OK);
    }

    @GetMapping("/CategoriaGasto")
    public ResponseEntity<List<CategoriaGastos>> getCategoriaGastos(){
        List<CategoriaGastos> listCategoriaGastos = categoriaGastosService.getCategoriasGastos();
        return new ResponseEntity<>(listCategoriaGastos, HttpStatus.OK);
    }


    @PostMapping("/CategoriaGrupo/{nombreTipoGrupo}")
    public ResponseEntity<Object> getCreateCategoriaGrupos(@PathVariable String nombreTipoGrupo){
        try {
            CategoriaGrupos categoriaGrupos = categoriaGruposService.postCreateCategoriaGrupos(nombreTipoGrupo);
            return new ResponseEntity<>(categoriaGrupos, HttpStatus.OK);
        }catch (DataIntegrityViolationException e) {
            // Manejar la excepción de violación de unicidad
            return new ResponseEntity<>("Error: El nombre de tipo de grupo ya está registrado.", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            // Manejar otras excepciones
            return new ResponseEntity<>("Error interno del servidor", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/GrupoAmigos/{idUsuario}")
    public ResponseEntity<List<GrupoAmigos>> getObtenerGruposPorUsuario(@PathVariable String idUsuario){
        List<GrupoAmigos> listGrupoAmigos = grupoAmigosService.getGrupoAmigos(Long.parseLong(idUsuario));
        return new ResponseEntity<>(listGrupoAmigos, HttpStatus.OK);
    }

    @PostMapping("/GrupoAmigos/{idCategoriaGrupos}/{nombreGrupo}/{idUsuario}")
    public ResponseEntity<Object> postCrearGrupoAmigos(@PathVariable String idCategoriaGrupos, @PathVariable String nombreGrupo, @PathVariable String idUsuario){
        GrupoAmigos grupoAmigos = new GrupoAmigos();

        Optional<CategoriaGrupos> categoriaGrupos = categoriaGruposService.getObtenerCategoriaGrupos(idCategoriaGrupos);
        if (categoriaGrupos.isEmpty()){
            return new ResponseEntity<>("Error: No se encuentra categoria grupo.", HttpStatus.BAD_REQUEST);
        }
        grupoAmigos.setNombreGrupo(nombreGrupo);
        grupoAmigos.setCategoria(categoriaGrupos.get());
        try {
            return new ResponseEntity<>(grupoAmigosService.postCreateGrupoAmigos(grupoAmigos,Long.parseLong(idUsuario)),HttpStatus.OK);
        }catch (DataIntegrityViolationException e) {
            // Manejar la excepción de violación de unicidad
            return new ResponseEntity<>("Error: El nombre del grupo ya está registrado.", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            // Manejar otras excepciones
            return new ResponseEntity<>("Error interno del servidor", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/GrupoAmigos/{idGrupoAmigos}")
    public ResponseEntity<Object> putActualizarGrupoAgmios(@PathVariable String idGrupoAmigos, @RequestBody GrupoDto grupoDto){
        Optional<CategoriaGrupos> categoriaGrupos = categoriaGruposService.getObtenerCategoriaGrupos(grupoDto.getIdCategoria());
        if (categoriaGrupos.isEmpty()){
            return new ResponseEntity<>("Error: No se encuentra categoria grupo.", HttpStatus.BAD_REQUEST);
        }

        try {
            GrupoAmigos grupoActualizado = grupoAmigosService.putActualizarGrupoAmigos(idGrupoAmigos, grupoDto, categoriaGrupos.get());

            if(grupoActualizado.equals(null)){
                return ResponseEntity.notFound().build();
            }else{
                return ResponseEntity.ok(grupoActualizado);
            }
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Error interno del servidor");
        }
    }

    @PostMapping("/CategoriaGasto/{nombreTipoGasto}")
    public ResponseEntity<Object> getCreateCategoriaGastos(@PathVariable String nombreTipoGasto){
        try {
            CategoriaGastos categoriaGastos = categoriaGastosService.postCreateCategoriaGastos(nombreTipoGasto);
            return new ResponseEntity<>(categoriaGastos, HttpStatus.OK);
        }catch (DataIntegrityViolationException e) {
            // Manejar la excepción de violación de unicidad
            return new ResponseEntity<>("Error: El nombre de categoria gasto ya está registrado.", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            // Manejar otras excepciones
            return new ResponseEntity<>("Error interno del servidor", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/Gasto")
    public ResponseEntity<Object> postCrearGasto(@RequestBody GastoDTO gastoDto){
        Optional<CategoriaGastos> categoriaGastos= categoriaGastosService.getObtenerCategoriaGastos(gastoDto.getIdCategoriaGastos().toString());
        if (categoriaGastos.isEmpty()){
            return new ResponseEntity<>("Error: No se encuentra categoria gastos.", HttpStatus.BAD_REQUEST);
        }

        GastoP responseGasto = gastoService.postCrearGasto(gastoDto,categoriaGastos.get());
        if (responseGasto == null) {
            return new ResponseEntity<>("Error interno del servidor", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(responseGasto,HttpStatus.OK);
    }

    @PutMapping("/Gasto/{idGasto}")
    public ResponseEntity<Object> putActualizarGasto(@PathVariable String idGasto, @RequestBody GastoDTO gastoDTO){
        Optional<CategoriaGastos> categoriaGastos= categoriaGastosService.getObtenerCategoriaGastos(gastoDTO.getIdCategoriaGastos().toString());
        if (categoriaGastos.isEmpty()){
            return new ResponseEntity<>("Error: No se encuentra categoria gastos.", HttpStatus.BAD_REQUEST);
        }
        GastoP responseGasto = gastoService.putGasto(Long.parseLong(idGasto),gastoDTO,categoriaGastos.get());
        return new ResponseEntity<>(responseGasto,HttpStatus.OK);
    }

    @GetMapping("/Gasto/GastoGrupal/{idGrupo}")
    public ResponseEntity<List<GastoGrupal>> getObtenerTodosLosGastoGrupal(@PathVariable String idGrupo){
        List<GastoGrupal> gastoPList = gastoService.getObtenerGastosGrupal(Long.parseLong(idGrupo));
        return new ResponseEntity<>(gastoPList,HttpStatus.OK);
    }

    @GetMapping("/Gasto/GastoIndividual/{idUsuario}")
    public ResponseEntity<List<GastoIndividual>> getObtenerTodosLosGastoIndividual(@PathVariable String idUsuario){
        List<GastoIndividual> gastoPList = gastoService.getObtenerGastosIndividual(Long.parseLong(idUsuario));
        return new ResponseEntity<>(gastoPList,HttpStatus.OK);
    }


    //AGREGAR COMO AMIGOS, ENVIAR SOLICITUD, LAS ACEPTADAS SON LAS QUE SE HIZO AMIGO.
    @PostMapping("/Solicitud")
    public ResponseEntity<Object> postCrearSolicitud(@RequestBody SolicitudDTO solicitudDTO ){
        Solicitud solicitudResponse = solicitudService.postCrearSolicitud(solicitudDTO);
        return new ResponseEntity<>(solicitudResponse,HttpStatus.OK);
    }

    @PutMapping("/Solicitud/{idSolcitud}/{estado}")
    public ResponseEntity<Object> putModificarSolicitud(@PathVariable String idSolcitud, @PathVariable String estado){
        Solicitud solicitudResponse = solicitudService.putModificarSolicitud(Long.parseLong(idSolcitud),estado);


        return new ResponseEntity<>(solicitudResponse,HttpStatus.OK);
    }

    @GetMapping("/Solicitud/{email}")
    public ResponseEntity<List<Solicitud>> getSolicitudes(@PathVariable String email){
        List<Solicitud> solicitudes = solicitudService.getSolicitudes(email);

        return new ResponseEntity<>(solicitudes,HttpStatus.OK);
    }

}
