package com.ttps.CuentasClarastp.repository;

import com.ttps.CuentasClarastp.modelsDB.GrupoAmigos;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GrupoAmigosRepository extends JpaRepository<GrupoAmigos,Long> {

    List<GrupoAmigos> findAllByDueno(Long idUsuario);

    Optional<GrupoAmigos> findByNombreGrupo(String nombreGrupo);
}
