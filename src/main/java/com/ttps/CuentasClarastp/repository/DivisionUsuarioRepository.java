package com.ttps.CuentasClarastp.repository;

import com.ttps.CuentasClarastp.modelsDB.DivisionUsuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DivisionUsuarioRepository extends JpaRepository<DivisionUsuario, Long> {

    @Override
    DivisionUsuario save(DivisionUsuario entity);

}
