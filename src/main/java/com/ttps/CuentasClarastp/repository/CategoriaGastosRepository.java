package com.ttps.CuentasClarastp.repository;

import com.ttps.CuentasClarastp.modelsDB.CategoriaGastos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaGastosRepository extends JpaRepository<CategoriaGastos,Long> {
}
