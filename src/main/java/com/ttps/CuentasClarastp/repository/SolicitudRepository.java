package com.ttps.CuentasClarastp.repository;

import com.ttps.CuentasClarastp.modelsDB.Solicitud;
import com.ttps.CuentasClarastp.modelsDB.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SolicitudRepository extends JpaRepository<Solicitud, Long> {

    @Override
    Solicitud save(Solicitud entity);


    Optional<Solicitud> getByIDSolicitud(Long idSolicitud);

    List<Solicitud> findAllByDestinatarioEmail(String email);
}
