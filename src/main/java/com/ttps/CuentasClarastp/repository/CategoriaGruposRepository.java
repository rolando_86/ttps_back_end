package com.ttps.CuentasClarastp.repository;

import com.ttps.CuentasClarastp.modelsDB.CategoriaGrupos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoriaGruposRepository extends JpaRepository<CategoriaGrupos,Long> {

    @Override
    CategoriaGrupos save(CategoriaGrupos entity);

}
