package com.ttps.CuentasClarastp.repository;

import com.ttps.CuentasClarastp.modelsDB.Login;
import com.ttps.CuentasClarastp.modelsDB.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsuarioRepository extends JpaRepository<Usuario,Long> {

    @Override
    Usuario save(Usuario entity);

    Usuario findByEmailAndContrasena(String email, String contrasena);

    Optional<Usuario> findByEmail(String email);

}
