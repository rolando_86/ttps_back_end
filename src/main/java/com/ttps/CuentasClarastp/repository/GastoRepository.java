package com.ttps.CuentasClarastp.repository;

import com.ttps.CuentasClarastp.modelsDB.GastoP;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GastoRepository extends JpaRepository<GastoP,Long> {
}
