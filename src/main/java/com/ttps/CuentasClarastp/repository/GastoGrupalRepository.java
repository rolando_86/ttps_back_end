package com.ttps.CuentasClarastp.repository;

import com.ttps.CuentasClarastp.modelsDB.GastoGrupal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GastoGrupalRepository extends JpaRepository<GastoGrupal,Long> {
    List<GastoGrupal> findAllByIdUsuario(Long idUsuario);
}
