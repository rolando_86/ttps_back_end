package com.ttps.CuentasClarastp.repository;

import com.ttps.CuentasClarastp.modelsDB.GastoIndividual;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GastoIndividualRepository extends JpaRepository<GastoIndividual,Long> {
    List<GastoIndividual> findAllByIdUsuarioReceptor(Long idUsuario);
    List<GastoIndividual> findAllByIdUsuarioEmisor(Long idUsuario);
}
