package com.ttps.CuentasClarastp.service;

import com.ttps.CuentasClarastp.modelsDB.DTO.SolicitudDTO;
import com.ttps.CuentasClarastp.modelsDB.Solicitud;

import java.util.List;

public interface SolicitudService {
    Solicitud postCrearSolicitud(SolicitudDTO solicitudDTO);

    Solicitud putModificarSolicitud(Long idSolicitud, String estado);

    List<Solicitud> getSolicitudes(String email);
}
