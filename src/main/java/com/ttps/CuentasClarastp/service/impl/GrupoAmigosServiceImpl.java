package com.ttps.CuentasClarastp.service.impl;

import com.ttps.CuentasClarastp.modelsDB.CategoriaGrupos;
import com.ttps.CuentasClarastp.modelsDB.DTO.GrupoDto;
import com.ttps.CuentasClarastp.modelsDB.GrupoAmigos;
import com.ttps.CuentasClarastp.modelsDB.Usuario;
import com.ttps.CuentasClarastp.repository.GrupoAmigosRepository;
import com.ttps.CuentasClarastp.repository.UsuarioRepository;
import com.ttps.CuentasClarastp.service.GrupoAmigosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GrupoAmigosServiceImpl implements GrupoAmigosService {

    @Autowired
    private GrupoAmigosRepository grupoAmigosRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public GrupoAmigos postCreateGrupoAmigos(GrupoAmigos grupoAmigos, Long idUsuario) {
        Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
        if(usuario.isPresent()){
            grupoAmigos.setDueno(idUsuario);
            List<Usuario> listUsuario = new ArrayList<>();
            listUsuario.add(usuario.get());
            grupoAmigos.setUsuarios(listUsuario);
            return grupoAmigosRepository.save(grupoAmigos);
        }
        return null;

    }

    @Override
    public GrupoAmigos putActualizarGrupoAmigos(String idGrupoAmigos, GrupoDto grupoDto, CategoriaGrupos categoriaGrupos) {

        Optional<GrupoAmigos> grupoAmigosAux = grupoAmigosRepository.findById(Long.valueOf(idGrupoAmigos))
                .map(grupoExistente -> {
                    grupoExistente.setNombreGrupo(grupoDto.getNombreGrupo());
                    grupoExistente.setCategoria(categoriaGrupos);
                    return grupoAmigosRepository.save(grupoExistente);
                });
        return grupoAmigosAux.orElse(null);
    }

    @Override
    public List<GrupoAmigos> getGrupoAmigos(Long idUsuario) {

        return grupoAmigosRepository.findAllByDueno(idUsuario);
    }
}
