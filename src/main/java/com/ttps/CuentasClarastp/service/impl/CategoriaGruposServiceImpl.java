package com.ttps.CuentasClarastp.service.impl;

import com.ttps.CuentasClarastp.modelsDB.CategoriaGrupos;
import com.ttps.CuentasClarastp.repository.CategoriaGruposRepository;
import com.ttps.CuentasClarastp.service.CategoriaGruposService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoriaGruposServiceImpl implements CategoriaGruposService {

    @Autowired
    private CategoriaGruposRepository categoriaGruposRepository;

    @Override
    public CategoriaGrupos postCreateCategoriaGrupos(String nombreCategoria) {
        CategoriaGrupos categoriaGrupos = new CategoriaGrupos();
        categoriaGrupos.setNombreTipoGrupo(nombreCategoria);
        return categoriaGruposRepository.save(categoriaGrupos);
    }

    @Override
    public Optional<CategoriaGrupos> getObtenerCategoriaGrupos(String id) {
        return categoriaGruposRepository.findById(Long.valueOf(id));
    }

    @Override
    public List<CategoriaGrupos> getCategoriasGrupos() {
        return categoriaGruposRepository.findAll();
    }
}
