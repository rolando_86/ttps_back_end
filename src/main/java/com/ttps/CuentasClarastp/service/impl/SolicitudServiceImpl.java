package com.ttps.CuentasClarastp.service.impl;

import com.ttps.CuentasClarastp.modelsDB.DTO.SolicitudDTO;
import com.ttps.CuentasClarastp.modelsDB.Solicitud;
import com.ttps.CuentasClarastp.modelsDB.Usuario;
import com.ttps.CuentasClarastp.repository.SolicitudRepository;
import com.ttps.CuentasClarastp.repository.UsuarioRepository;
import com.ttps.CuentasClarastp.service.SolicitudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SolicitudServiceImpl implements SolicitudService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private SolicitudRepository solicitudRepository;


    @Override
    public Solicitud postCrearSolicitud(SolicitudDTO solicitudDTO) {
        Optional<Usuario> usuario = usuarioRepository.findByEmail(solicitudDTO.getDestinatarioEmail());
        if(usuario.isPresent()){
            Solicitud solicitud = new Solicitud();
            solicitud.setEstado("");
            solicitud.setUsuario(usuario.get());
            solicitud.setRemitenteEmail(solicitudDTO.getRemitenteEmail());
            solicitud.setDestinatarioEmail(solicitudDTO.getDestinatarioEmail());
            Solicitud solicitudRepo = solicitudRepository.save(solicitud);
            return solicitudRepo;
        }
        return null;
    }

    @Override
    public Solicitud putModificarSolicitud(Long idSolicitud, String estado) {
        Optional<Solicitud> solicitud = solicitudRepository.getByIDSolicitud(idSolicitud);
        if(solicitud.isPresent()){
            Solicitud solicitudAux = solicitud.get();
            solicitudAux.setEstado(estado);
            return solicitudRepository.save(solicitudAux);
        }
        return null;
    }

    @Override
    public List<Solicitud> getSolicitudes(String email) {
        Optional<Usuario> usuario = usuarioRepository.findByEmail(email);
        if(usuario.isPresent()){
            return solicitudRepository.findAllByDestinatarioEmail(usuario.get().getEmail());
        }
        return null;
    }
}
