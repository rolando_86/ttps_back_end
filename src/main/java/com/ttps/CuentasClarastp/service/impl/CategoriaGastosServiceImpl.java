package com.ttps.CuentasClarastp.service.impl;

import com.ttps.CuentasClarastp.modelsDB.CategoriaGastos;
import com.ttps.CuentasClarastp.repository.CategoriaGastosRepository;
import com.ttps.CuentasClarastp.service.CategoriaGastosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoriaGastosServiceImpl implements CategoriaGastosService {

    @Autowired
    private CategoriaGastosRepository categoriaGastosRepository;

    @Override
    public CategoriaGastos postCreateCategoriaGastos(String nombreTipoGasto) {
        CategoriaGastos categoriaGastos = new CategoriaGastos();
        categoriaGastos.setNombreTipoGasto(nombreTipoGasto);
        return categoriaGastosRepository.save(categoriaGastos);
    }

    public Optional<CategoriaGastos> getObtenerCategoriaGastos(String id) {
        return categoriaGastosRepository.findById(Long.valueOf(id));
    }

    @Override
    public List<CategoriaGastos> getCategoriasGastos() {
        return categoriaGastosRepository.findAll();
    }
}
