package com.ttps.CuentasClarastp.service.impl;

import com.ttps.CuentasClarastp.modelsDB.*;
import com.ttps.CuentasClarastp.modelsDB.DTO.GastoDTO;
import com.ttps.CuentasClarastp.repository.*;
import com.ttps.CuentasClarastp.service.GastoService;
import models.Gasto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GastoServiceImpl implements GastoService {

    @Autowired
    private GastoRepository gastoRepository;

    @Autowired
    private GastoGrupalRepository gastoGrupalRepository;

    @Autowired
    private GastoIndividualRepository gastoIndividualRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private GrupoAmigosRepository grupoAmigosRepository;

    @Autowired
    private DivisionUsuarioRepository divisionUsuarioRepository;

    private GastoIndividual mappingGastoIndividualDtoToGasto(GastoDTO gastoDto, GastoIndividual gastoIndividual){

        gastoIndividual.setMontoGasto(gastoDto.getMontoGasto());
        gastoIndividual.setFechaGasto(gastoDto.getFechaGasto());
        gastoIndividual.setImagenAsociada(gastoDto.getImagenAsociada());
        gastoIndividual.setEmailGastoIntegrante(gastoDto.getEmailGastoIntegrante());
        gastoIndividual.setFormaDividirGasto(gastoDto.getFormaDividirGasto());
        gastoIndividual.setIdUsuarioEmisor(gastoDto.getIdUsuarioEmisor());
        gastoIndividual.setIdUsuarioReceptor(gastoDto.getIdUsuarioReceptor());
        return gastoIndividual;
    }

    private GastoGrupal mappingGastoGrupalDtoToGasto(GastoDTO gastoDto, GastoGrupal gastoGrupal, Long idGrupo){

        gastoGrupal.setMontoGasto(gastoDto.getMontoGasto());
        gastoGrupal.setFechaGasto(gastoDto.getFechaGasto());
        gastoGrupal.setImagenAsociada(gastoDto.getImagenAsociada());
        gastoGrupal.setFormaDividirGasto(gastoDto.getFormaDividirGasto());
        gastoGrupal.setEmailGastoIntegrante(gastoDto.getEmailGastoIntegrante());
        gastoGrupal.setIdUsuario(gastoDto.getIdUsuario());
        gastoGrupal.setIdGrupoAmigos(idGrupo);
        gastoGrupal.setFormaDividirGasto(gastoDto.getFormaDividirGasto());
        return gastoGrupal;
    }

    @Override
    public GastoP postCrearGasto(GastoDTO gastoDto, CategoriaGastos categoriaGastos) {
        Optional<Usuario> usuario = usuarioRepository.findByEmail(gastoDto.getEmailGastoIntegrante());
        Optional<GrupoAmigos> grupoAmigos = grupoAmigosRepository.findByNombreGrupo(gastoDto.getNombreGrupo());
        if(usuario.isPresent() && grupoAmigos.isPresent()){
            if(gastoDto.getTipoGasto().equals("Individual")){
                GastoIndividual gastoIndividual = new GastoIndividual();
                gastoIndividual = this.mappingGastoIndividualDtoToGasto(gastoDto,gastoIndividual);
                gastoIndividual.setCategoriaGasto(categoriaGastos);
                return gastoRepository.save(gastoIndividual);
            }else{
                GastoGrupal gastoGrupal = new GastoGrupal();

                DivisionUsuario divisionUsuario = new DivisionUsuario();
                divisionUsuario.setIdUsuario(gastoDto.getIdUsuario());
                divisionUsuario.setValor(gastoDto.getValor());
                DivisionUsuario divisionUsuarioIngreso = divisionUsuarioRepository.save(divisionUsuario);

                List<DivisionUsuario> list = new ArrayList<>();
                list.add(divisionUsuarioIngreso);


                gastoGrupal = this.mappingGastoGrupalDtoToGasto(gastoDto, gastoGrupal, grupoAmigos.get().getIdGrupoAmigos());
                gastoGrupal.setCategoriaGasto(categoriaGastos);
                gastoGrupal.setListDivisionUsuario(list);

                return gastoRepository.save(gastoGrupal);
            }
        }
        return null;
    }


    public List<GastoGrupal> getObtenerGastosGrupal(Long idUsuario) {
        List<GastoGrupal> listGastoGrupal = gastoGrupalRepository.findAllByIdUsuario(idUsuario);
        return listGastoGrupal;
    }

    @Override
    public List<GastoIndividual> getObtenerGastosIndividual(Long idUsuario) {
        List<GastoIndividual> listGastoIndividual = gastoIndividualRepository.findAllByIdUsuarioEmisor(idUsuario);
        return listGastoIndividual;
    }

    @Override
    public GastoP putGasto(Long idGasto, GastoDTO gastoDto, CategoriaGastos categoriaGastos) {
        Optional<Usuario> usuario = usuarioRepository.findByEmail(gastoDto.getEmailGastoIntegrante());
        Optional<GrupoAmigos> grupoAmigos = grupoAmigosRepository.findByNombreGrupo(gastoDto.getNombreGrupo());
        if(usuario.isPresent() && grupoAmigos.isPresent()){
            Optional<GastoP> gastoP = gastoRepository.findById(idGasto);
            if(gastoP.isPresent()){
                if(gastoDto.getTipoGasto().equals("Individual")){
                    GastoIndividual gastoIndividual = this.mappingGastoIndividualDtoToGasto(gastoDto, (GastoIndividual) gastoP.get());
                    gastoIndividual.setCategoriaGasto(categoriaGastos);
                    return gastoRepository.save(gastoIndividual);
                }else{
                    GastoGrupal gastoGrupalAux = new GastoGrupal();

                    DivisionUsuario divisionUsuario = new DivisionUsuario();
                    divisionUsuario.setIdUsuario(gastoDto.getIdUsuario());
                    divisionUsuario.setValor(gastoDto.getValor());
                    DivisionUsuario divisionUsuarioIngreso = divisionUsuarioRepository.save(divisionUsuario);

                    List<DivisionUsuario> list = new ArrayList<>();
                    list.add(divisionUsuarioIngreso);


                    gastoGrupalAux = this.mappingGastoGrupalDtoToGasto(gastoDto, (GastoGrupal) gastoP.get(), ((GastoGrupal) gastoP.get()).getIdGrupoAmigos());
                    gastoGrupalAux.setCategoriaGasto(categoriaGastos);
                    gastoGrupalAux.setListDivisionUsuario(list);

                    return gastoRepository.save(gastoGrupalAux);
                }
            }
        }
        return null;
    }
}
