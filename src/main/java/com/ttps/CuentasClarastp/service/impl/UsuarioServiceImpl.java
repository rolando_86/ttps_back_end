package com.ttps.CuentasClarastp.service.impl;

import com.ttps.CuentasClarastp.modelsDB.Login;
import com.ttps.CuentasClarastp.modelsDB.Usuario;
import com.ttps.CuentasClarastp.repository.UsuarioRepository;
import com.ttps.CuentasClarastp.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Usuario postCreateUsuario(Usuario usuario) {
        return usuarioRepository.save(usuario);
    }

    @Override
    public Usuario getLogin(Login login) {
        return usuarioRepository.findByEmailAndContrasena(login.getEmail(), login.getContrasena());
    }

}
