package com.ttps.CuentasClarastp.service;

import com.ttps.CuentasClarastp.modelsDB.CategoriaGrupos;
import com.ttps.CuentasClarastp.modelsDB.DTO.GrupoDto;
import com.ttps.CuentasClarastp.modelsDB.GrupoAmigos;

import java.util.List;

public interface GrupoAmigosService {
    GrupoAmigos postCreateGrupoAmigos(GrupoAmigos grupoAmigos, Long idUsuario);

    GrupoAmigos putActualizarGrupoAmigos(String idGrupoAmigos, GrupoDto grupoDto, CategoriaGrupos categoriaGrupos);

    List<GrupoAmigos> getGrupoAmigos(Long idUsuario);
}
