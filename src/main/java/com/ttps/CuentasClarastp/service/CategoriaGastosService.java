package com.ttps.CuentasClarastp.service;

import com.ttps.CuentasClarastp.modelsDB.CategoriaGastos;

import java.util.List;
import java.util.Optional;

public interface CategoriaGastosService {
    CategoriaGastos postCreateCategoriaGastos(String nombreTipoGasto);

    Optional<CategoriaGastos> getObtenerCategoriaGastos(String id);

    List<CategoriaGastos> getCategoriasGastos();
}
