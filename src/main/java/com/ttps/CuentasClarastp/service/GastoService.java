package com.ttps.CuentasClarastp.service;

import com.ttps.CuentasClarastp.modelsDB.CategoriaGastos;
import com.ttps.CuentasClarastp.modelsDB.DTO.GastoDTO;
import com.ttps.CuentasClarastp.modelsDB.GastoGrupal;
import com.ttps.CuentasClarastp.modelsDB.GastoIndividual;
import com.ttps.CuentasClarastp.modelsDB.GastoP;

import java.util.List;

public interface GastoService {
    GastoP postCrearGasto(GastoDTO gastoDto, CategoriaGastos categoriaGastos);

    List<GastoGrupal> getObtenerGastosGrupal(Long idGrupo);

    List<GastoIndividual> getObtenerGastosIndividual(Long idUsuario);

    GastoP putGasto(Long idGasto, GastoDTO gasto, CategoriaGastos categoriaGastos);
}
