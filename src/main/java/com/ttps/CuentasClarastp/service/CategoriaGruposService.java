package com.ttps.CuentasClarastp.service;

import com.ttps.CuentasClarastp.modelsDB.CategoriaGrupos;

import java.util.List;
import java.util.Optional;

public interface CategoriaGruposService {
    CategoriaGrupos postCreateCategoriaGrupos(String nombreCategoria);

    Optional<CategoriaGrupos> getObtenerCategoriaGrupos(String id);

    List<CategoriaGrupos> getCategoriasGrupos();
}
