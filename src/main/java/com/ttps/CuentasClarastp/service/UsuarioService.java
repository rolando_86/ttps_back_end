package com.ttps.CuentasClarastp.service;


import com.ttps.CuentasClarastp.modelsDB.Login;
import com.ttps.CuentasClarastp.modelsDB.Usuario;

public interface UsuarioService {
    Usuario postCreateUsuario(Usuario usuario);

    Usuario getLogin(Login login);
}
