package com.ttps.CuentasClarastp.modelsDB;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import models.Gasto;

@Entity
@Table(name = "GastoGrupal")
@DiscriminatorValue("GRUPAL")
@Getter
@Setter
@RequiredArgsConstructor
public class GastoGrupal extends GastoP {

    @Column(name = "IDUsuario")
    private Long idUsuario;

    @Column(name = "IDGrupoAmigos")
    private Long idGrupoAmigos;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}
