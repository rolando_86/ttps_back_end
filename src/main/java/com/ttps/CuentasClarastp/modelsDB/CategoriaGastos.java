package com.ttps.CuentasClarastp.modelsDB;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "CategoriaGastos")
@Getter
@Setter
@RequiredArgsConstructor
public class CategoriaGastos {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "IDCategoriaGastos")
    private Long idCategoriaGastos;

    @Column(name = "nombreTipoGasto", unique = true)
    private String nombreTipoGasto;

    @OneToMany(mappedBy="CategoriaGasto")
    @JsonIgnore
    private List<GastoP> listGasto;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}
