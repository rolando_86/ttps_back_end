package com.ttps.CuentasClarastp.modelsDB;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "GastoP")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name="TIPO_GASTO")
@Getter
@Setter
@RequiredArgsConstructor
public class GastoP {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDGasto")
    private Long idGasto;

    @Column(name = "montoGasto")
    private BigDecimal montoGasto;

    @Column(name = "fechaGasto")
    @Temporal(TemporalType.DATE)
    private Date fechaGasto;

    @Column(name = "imagenAsociada")
    private String imagenAsociada;

    @Column(name = "formaDividirGasto")
    private String formaDividirGasto;

    @Column(name = "emailGastoIntegrante")
    private String emailGastoIntegrante;

    @ManyToOne
    @JoinColumn(name="IDCategoriaGastos")
    private CategoriaGastos CategoriaGasto;

    @OneToMany(mappedBy="gastoP")
    private List<DivisionUsuario> listDivisionUsuario;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}