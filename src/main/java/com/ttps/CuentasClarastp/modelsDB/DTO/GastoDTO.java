package com.ttps.CuentasClarastp.modelsDB.DTO;

import jakarta.persistence.Column;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
@Getter
@Setter
@RequiredArgsConstructor
public class GastoDTO {
    private BigDecimal montoGasto;
    private Date fechaGasto;
    private String imagenAsociada;
    private String formaDividirGasto;
    private String emailGastoIntegrante;
    private Long idCategoriaGastos;
    private Long idUsuarioEmisor;
    private Long idUsuarioReceptor;
    private Long idUsuario;
    private String nombreGrupo;
    private String tipoGasto;
    private BigDecimal valor;
}
