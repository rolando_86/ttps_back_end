package com.ttps.CuentasClarastp.modelsDB.DTO;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class SolicitudDTO {

    private String remitenteEmail;

    private String destinatarioEmail;

    private String estado;//ACEPTADO O RECHAZADO

}
