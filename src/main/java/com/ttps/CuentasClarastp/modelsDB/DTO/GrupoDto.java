package com.ttps.CuentasClarastp.modelsDB.DTO;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class GrupoDto {
    private String idCategoria;

    private String nombreGrupo;

}
