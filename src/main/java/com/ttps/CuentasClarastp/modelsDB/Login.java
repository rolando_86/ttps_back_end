package com.ttps.CuentasClarastp.modelsDB;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class Login {
    private String email;
    private String contrasena;
}
