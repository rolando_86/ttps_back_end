package com.ttps.CuentasClarastp.modelsDB;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "GrupoAmigos")
@Getter
@Setter
@RequiredArgsConstructor
public class GrupoAmigos {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDGrupoAmigos")
    private Long idGrupoAmigos;

    @Column(name = "nombreGrupo", unique = true)
    private String nombreGrupo;

    @Column(name = "dueno")
    private Long dueno;

    @ManyToOne
    @JoinColumn(name="IDCategoriaGrupos")
    private CategoriaGrupos Categoria;

    @ManyToMany
    @JoinTable(
            name = "usuario_grupo",
            joinColumns = @JoinColumn(name = "dueno"),
            inverseJoinColumns = @JoinColumn(name = "IDUsuario")
    )
    private List<Usuario> usuarios;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}