package com.ttps.CuentasClarastp.modelsDB;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "Usuario")
public class Usuario {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "IDUsuario")
    private Long idUsuario;

    @Column(name = "nombreUsuario")
    private String nombreUsuario;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "nombres")
    private String nombres;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "contrasena")
    private String contrasena;

    @Column(name = "saldoTotal")
    private BigDecimal saldoTotal;

    @OneToMany(mappedBy="Usuario")
    @JsonIgnore
    private List<Solicitud> listSolicitudes;

    @ManyToMany(mappedBy = "usuarios")
    @JsonIgnore
    private List<GrupoAmigos> gruposAmigos;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}