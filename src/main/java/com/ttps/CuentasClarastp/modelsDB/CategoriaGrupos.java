package com.ttps.CuentasClarastp.modelsDB;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "CategoriaGrupos")
@Getter
@Setter
@RequiredArgsConstructor
public class CategoriaGrupos {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDCategoriaGrupos")
    private Long idCategoriaGrupos;

    @Column(name = "nombreTipoGrupo", unique = true)
    private String nombreTipoGrupo;

    @OneToMany(mappedBy="Categoria")
    @JsonIgnore
    private List<GrupoAmigos> listGrupoAmigos;


    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}