package com.ttps.CuentasClarastp.modelsDB;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import models.Gasto;

@Entity
@Table(name = "GastoIndividual")
@DiscriminatorValue("INDIVIDUAL")
@Getter
@Setter
@RequiredArgsConstructor
public class GastoIndividual extends GastoP{

    @Column(name = "IDUsuarioEmisor")
    private Long idUsuarioEmisor;

    @Column(name = "IDUsuarioReceptor")
    private Long idUsuarioReceptor;

    // Constructor, getters y setters se esta usando Lombok

    // Otras operaciones y métodos de la clase se esta usando lombok
}
