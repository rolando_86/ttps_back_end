package com.ttps.CuentasClarastp.modelsDB;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Entity
@Table(name = "DivisionUsuario")
@Getter
@Setter
@RequiredArgsConstructor
public class DivisionUsuario {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "IDDivisionUsuario")
    private Long idDivisionUsuario;

    @Column(name = "IDUsuario")
    private Long idUsuario;

    @Column(name = "valor")
    private BigDecimal valor;

    @ManyToOne
    @JoinColumn(name="IDGasto")
    @JsonIgnore
    private GastoP gastoP;

}
