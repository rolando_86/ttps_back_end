package com.ttps.CuentasClarastp.modelsDB;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@Entity
@Table(name = "Solicitud")
public class Solicitud {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "IDSolicitud")
    private Long IDSolicitud;

    @Column(name = "remitenteEmail")
    private String remitenteEmail;

    @Column(name = "destinatarioEmail")
    private String destinatarioEmail;

    @Column(name = "estado")
    private String estado;//ACEPTADO O RECHAZADO

    @ManyToOne
    @JoinColumn(name="IDUsuario")
    private Usuario Usuario;

}
