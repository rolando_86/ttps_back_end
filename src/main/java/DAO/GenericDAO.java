package DAO;

import java.util.List;

public interface GenericDAO<T> {
	
	public T modify (T entity);
	public void delete (T entity);
	public T delete(Long id);
	public T exists(Long id);
	public T create(T entity);
	public T find(Long id);
	public List<T> findAll(String column);
	
}
