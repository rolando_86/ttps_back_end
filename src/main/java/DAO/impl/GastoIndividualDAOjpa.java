package DAO.impl;

import models.GastoIndividual;
import DAO.GastoIndividualDAO;

public class GastoIndividualDAOjpa extends GenericDAOHibernateJPA<GastoIndividual> implements GastoIndividualDAO {
    public GastoIndividualDAOjpa(Class<GastoIndividual> class1) {
        super(class1);
    }
}
