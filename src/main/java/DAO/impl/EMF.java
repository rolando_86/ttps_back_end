package DAO.impl;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

public class EMF {

	private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("cuentaBD");

	public static EntityManagerFactory getEMF() {
		return emf;
	}

}
