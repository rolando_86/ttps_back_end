package DAO.impl;

import java.util.List;

import DAO.GenericDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;

public class GenericDAOHibernateJPA<T> implements GenericDAO<T> {

	private static final Logger logger = LogManager.getLogger(GenericDAOHibernateJPA.class);

	protected Class<T> persistentClass;

	public GenericDAOHibernateJPA(Class<T> class1) {
		this.persistentClass = class1;
	}

	@Override
	public T create(T entity) {
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.persist(entity);
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null && tx.isActive())
				tx.rollback();
			logger.error("Error en create");
			throw e;
		} finally {
			em.close();
		}
		return entity;
	}

	@Override
	public void delete(T entity) {
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction tx = null;
		try {
			tx = em.getTransaction();
			tx.begin();
			em.remove(em.merge(entity));
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null && tx.isActive())
				tx.rollback();
			logger.error("Error en delete");
			throw e;
		} finally {
			em.close();
		}
	}

	@Override
	public T delete(Long id) {
		EntityManager em = EMF.getEMF().createEntityManager();
		T entity = em.find(this.persistentClass, id);
		if (entity != null) {
			EntityTransaction tx = null;
			try {
				tx = em.getTransaction();
				tx.begin();
				em.remove(em.merge(entity));
				tx.commit();
			} catch (RuntimeException e) {
				if (tx != null && tx.isActive())
					tx.rollback();
				logger.error("Error en delete con ID");
				throw e;
			}finally {
				em.close();
			}
		}
		return entity;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll(String column) {
		EntityManager em = EMF.getEMF().createEntityManager();
		String query = "SELECT e FROM " + this.persistentClass.getSimpleName() + " e";
		if (column != null && !column.isEmpty()) {
			query += " ORDER BY e." + column;
		}
		List<T> result = em.createQuery(query).getResultList();
		em.close();
		return  result;
	}

	@Override
	public T find(Long id) {
		EntityManager em = EMF.getEMF().createEntityManager();
		T result = em.find(persistentClass, id);
		em.close();
		return result;
	}

	@Override
	public T modify(T entity) {
		EntityManager em = EMF.getEMF().createEntityManager();
		EntityTransaction etx = em.getTransaction();
		etx.begin();
		T entityMerged = em.merge(entity);
		etx.commit();
		em.close();
		return entityMerged;
	}

	@Override
	public T exists(Long id) {
		EntityManager em = EMF.getEMF().createEntityManager();
		T result = em.find(persistentClass, id);
		em.close();
		return result;
	}

}
